#pragma once

#include <stm32f1xx_hal.h>

#include <cocoos.h>


extern "C" void HAL_SYSTICK_Callback(void)
{
    os_tick();
}

namespace cocoOS
{
void init()
{
    os_init();
}

// won't return
void start()
{
    os_start();
}
} // namespace cocoOS

#include <os_assert.c>
#include <os_cbk.c>
#include <os_event.c>
#include <os_kernel.c>
#include <os_msgqueue.c>
#include <os_sem.c>
#include <os_task.c>
