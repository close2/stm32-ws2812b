#include <stm32f1xx_hal.h>

#include "astm32_clock.hpp"
#include "astm32_gpio.hpp"

#include "astm32_spi1.hpp"
#include "astm32_spi_ws2812b.hpp"


const uint8_t NB_LED_BUFFER = 16;
const uint8_t NB_LEDS = 71;
const uint8_t NB_LED_TAIL = 70;
const uint8_t NB_BLOCKS = 5;

#define N_TASKS 4
#define N_QUEUES 4
#define N_SEMAPHORES 4
#define N_EVENTS 4

#include "cocoOS.hpp"

// See https://ioprog.com/2016/04/09/stm32f042-driving-a-ws2812b-using-spi/
// for the SPI explanation.


static SPI_HandleTypeDef spi_handle;

static DMA_HandleTypeDef spi_dma_tx_handle;

static uint8_t transmitBuffer[NB_LED_BUFFER * 3 * 3];  // 3 colors * 3 because of SPI "hack"
static Evt_t eventSpiTx;

volatile bool stopTransfer = false;


void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{
    if (hspi->Instance == SPIx) {
        // ## -a- Enable peripherals Clocks ##
        // We don't enable GPIO clock.  Automatically done in configureGPIO
        SPIx_CLK_ENABLE();

        // ## -b- Configure peripheral GPIO ##

        // SPI SCK GPIO pin configuration
        astm32::spi::configureGPIO<SPIx_MOSI_LINE>();
        // We don't need SCK output.
        //astm32::spi::configureGPIO<SPIx_SCK_LINE>();

        // ## -d- Configure the DMA ##
        // Configure the DMA handler for Transmission process
        DMAx_CLK_ENABLE();
        astm32::spi::configureDmaTX(spi_dma_tx_handle);
        // We will refill the buffer during transmission.
        spi_dma_tx_handle.Init.Mode = DMA_CIRCULAR;
        HAL_DMA_Init(&spi_dma_tx_handle);

        // Associate the initialized DMA handle to the the SPI handle
        __HAL_LINKDMA(hspi, hdmatx, spi_dma_tx_handle);

        // NVIC configuration for DMA transfer complete interrupt (SPIx_TX)
        HAL_NVIC_SetPriority(SPIx_DMA_TX_IRQn, 1, 1);
        HAL_NVIC_EnableIRQ(SPIx_DMA_TX_IRQn);
    }
}


void initSpi()
{
    spi_handle.Instance = SPIx;
    astm32::ws2812b::configureWS2812B(spi_handle);

    HAL_SPI_Init(&spi_handle);
}


// Irq handler for spi tx dma
extern "C" void SPIx_DMA_TX_IRQHandler(void)
{
    HAL_DMA_IRQHandler(spi_handle.hdmatx);
}

extern "C" void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef* hspi)
{
    if (hspi->Instance == SPIx) {
        if (stopTransfer) {
            HAL_SPI_DMAStop(hspi);
        }
        event_ISR_signal(eventSpiTx);
    }
}

extern "C" void HAL_SPI_TxHalfCpltCallback(SPI_HandleTypeDef* hspi)
{
    if (hspi->Instance == SPIx) {
        if (stopTransfer) {
            HAL_SPI_DMAStop(hspi);
        }
        event_ISR_signal(eventSpiTx);
    }
}

void initAll(void)
{
    HAL_Init();

    astm32::SystemClock::init();
    initSpi();

    cocoOS::init();
}

enum class Direction:uint8_t {
    Up,
    Down
};

static void calculateAndFillBuffer(uint8_t pos, uint8_t currentLed, Direction direction, uint8_t buffer[]) {
    const uint8_t dimmPLed = 0x7F / NB_LED_TAIL > 0 ? 0x7F / NB_LED_TAIL : 1;
    const uint8_t startBrightness = dimmPLed * NB_LED_TAIL;
    uint8_t green = 0;
    uint8_t blue = 0;
    uint8_t red = 0;
    if (pos == currentLed) {
        red = 0xFF;
    } else {
        uint16_t distance;
        if (direction == Direction::Up) {
            if (currentLed > pos) {
                distance = currentLed - pos;
            } else {
                distance = currentLed + pos;
            }
        } else {
            if (currentLed < pos) {
                distance = pos - currentLed;
            } else {
                distance = 2 * NB_LEDS - currentLed - pos;
            }
        }
        if (distance <= NB_LED_TAIL) {
            red = startBrightness - distance * dimmPLed;
        }
    }
    astm32::ws2812b::fillBuffer3Bytes(green, &(buffer[0]));
    astm32::ws2812b::fillBuffer3Bytes(red, &(buffer[3]));
    astm32::ws2812b::fillBuffer3Bytes(blue, &(buffer[6]));
}

static void fillWS2812BTask(void)
{
    static uint8_t currentLed = 0;
    static uint8_t ledRunner = 0;
    static Direction direction = Direction::Up;
    static uint8_t ledsTransmitted;
    static bool firstHalf;

    const uint8_t nbLedsPHalf = NB_LED_BUFFER / 2;  // 3 bytes / led
    static_assert(NB_LED_BUFFER % 2 == 0, "Buffer led count must be divisible by 2 (for DMA)");

    task_open();

    for (;;) {
        if (currentLed == 0 && direction == Direction::Down) {
            direction = Direction::Up;
        } else if (currentLed == NB_LEDS && direction == Direction::Up) {
            direction = Direction::Down;
        }

        currentLed += direction == Direction::Up ? 1 : -1;
        ledRunner = 0;
        firstHalf = true;
        stopTransfer = false;
        ledsTransmitted = 0;

        // Fill transmit buffer.
        for (uint8_t i = 0; i < NB_LED_BUFFER; i++) {
            calculateAndFillBuffer(ledRunner++, currentLed, direction, &(transmitBuffer[i * 3 * 3]));
        }

        HAL_SPI_Transmit_DMA(&spi_handle, transmitBuffer, sizeof(transmitBuffer));

        // Start spi tx.
        for (;;) {
            ledsTransmitted += nbLedsPHalf;

            // Prepare next batch.  (The next half)
            event_wait(eventSpiTx);
            // Half of buffer has been sent.

            // Replace with next batch.
            uint8_t start = firstHalf ? 0 : NB_LED_BUFFER / 2;
            uint8_t end = firstHalf ? NB_LED_BUFFER / 2 : NB_LED_BUFFER;
            if (ledsTransmitted < NB_LEDS) {
                for (uint8_t i = start; i < end; i++) {
                    calculateAndFillBuffer(ledRunner++, currentLed, direction, &(transmitBuffer[i * 3 * 3]));
                }
            } else {
                for (uint8_t i = start; i < end; i++) {
                    for (uint8_t j = 0; j < 9; j++) {
                        transmitBuffer[i * 9 + j] = 0;
                    }
                }
            }

            firstHalf = !firstHalf;
            if (ledsTransmitted >= NB_LEDS + nbLedsPHalf) break;
        }
        stopTransfer = true;
        event_wait(eventSpiTx);

        task_wait(7);
    }
    task_close();
}

int main(void)
{
    initAll();

    eventSpiTx = event_create();
    task_create(fillWS2812BTask, NULL, 1, NULL, 0, 0);

    cocoOS::start();

    return 0;
}
